//package com.atosyntel.calculator;

package com.atosyntel.calculator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.atosyntel.calculator.*;

@RunWith(JUnit4.class)
public class AppTest {
	private static Calculator calculator;
	// public static void initCalculator() {
	// calculator = new Calculator();
	// }

//	@BeforeClass
//	public void beforeEachTest() {
//		System.out.println("This is executed before each Test");
//	}
//
//	@AfterClass
//	public void afterEachTest() {
//		System.out.println("This is exceuted after each Test");
//	}

	@Test
	public void testSum() {
		calculator = new Calculator();
		int result = calculator.sum(3, 4);

		assertEquals(7, result);
	}

	@Test
	public void testDivison() {
		try {
			calculator = new Calculator();
			int result = calculator.division(10, 2);

			assertEquals(5, result);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	@Test(expected = Exception.class)
	public void testDivisionException() throws Exception {
		calculator = new Calculator();
		calculator.division(10, 0);
	}
	

	@Test
	public void testEqual() {
		calculator = new Calculator();
		boolean result = calculator.equalInteger(20, 20);

		assertTrue(result);
	}

	@Test
	public void testSubstraction() {
		int result = 10 - 3;

		assertFalse(result == 9);
	}
}
